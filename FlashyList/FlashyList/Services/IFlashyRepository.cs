﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FlashyList.Models;

namespace FlashyList.Services
{
    public interface IFlashyRepository
    {
        Task<int> SaveFlashTopic(FlashTopic flashTopicToSave);
        Task<ObservableCollection<FlashTopic>> GetAllFlashTopics();
    }
}