﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FlashyList.DependencyServices;
using FlashyList.Models;
using SQLite;

namespace FlashyList.Services
{
    public class FlashyRepository : IFlashyRepository
    {
        private IFileAccessService _fileAccessService;
        private SQLiteAsyncConnection _sqliteConnection;

        public FlashyRepository(IFileAccessService fileAccessService)
        {
            _fileAccessService = fileAccessService;

            var databaseFilePath = _fileAccessService.GetSqLiteDatabasePath("flashylist.db3");
            _sqliteConnection = new SQLiteAsyncConnection(databaseFilePath);
            CreateTablesSynchronously();

            Console.WriteLine($"**** {this.GetType().Name}.{nameof(FlashyRepository)}: ctor. databaseFilePath={databaseFilePath}");
        }

        private void CreateTablesSynchronously()
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(CreateTablesSynchronously)}");

            _sqliteConnection.CreateTableAsync<FlashTopic>().Wait();
        }

        public async Task<int> SaveFlashTopic(FlashTopic flashTopicToSave)
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(SaveFlashTopic)}:  {flashTopicToSave}");

            try
            {
                if (flashTopicToSave.Id == 0) // an Id of 0 indicates a new item, not yet saved.
                {
                    // This must be a NEW topic to insert
                    flashTopicToSave.Id = await _sqliteConnection.InsertAsync(flashTopicToSave);
                }
                else
                {
                    // This must be an EXISTING topic to update
                    await _sqliteConnection.UpdateAsync(flashTopicToSave);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"**** {this.GetType().Name}.{nameof(SaveFlashTopic)}:  EXCEPTION!! {ex}");
            }

            return flashTopicToSave.Id;
        }

        public async Task<ObservableCollection<FlashTopic>> GetAllFlashTopics()
        {
            var allTopics = new ObservableCollection<FlashTopic>();

            try
            {
                List<FlashTopic> topicList = await _sqliteConnection.Table<FlashTopic>().ToListAsync();
                allTopics = new ObservableCollection<FlashTopic>(topicList);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"**** {this.GetType().Name}.{nameof(GetAllFlashTopics)}:  EXCEPTION!! {ex}");
            }

            return allTopics;
        }
    }
}
