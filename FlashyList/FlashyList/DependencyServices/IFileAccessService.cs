﻿using System;

namespace FlashyList.DependencyServices
{
    public interface IFileAccessService
    {
        string GetSqLiteDatabasePath(string databaseName);
    }
}
