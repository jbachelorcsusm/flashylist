﻿using System;
using FlashyList.Models;
using FlashyList.Services;
using Prism.Commands;
using Prism.Navigation;

namespace FlashyList.ViewModels
{
    public class NewTopicPageViewModel : ViewModelBase
    {
        private IFlashyRepository _flashyRepository;

        public DelegateCommand SaveTopicCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }

        private FlashTopic _thisFlashTopic;
        public FlashTopic ThisFlashTopic
        {
            get { return _thisFlashTopic; }
            set { SetProperty(ref _thisFlashTopic, value); }
        }

        private string _flashTopicIdText;
        public string FlashTopicIdText
        {
            get { return _flashTopicIdText; }
            set { SetProperty(ref _flashTopicIdText, value); }
        }

        public NewTopicPageViewModel(INavigationService navigationService,
                                    IFlashyRepository flashyRepository)
            : base(navigationService)
        {
            Title = "New FlashTopic";
            _flashyRepository = flashyRepository;
            SaveTopicCommand = new DelegateCommand(OnSaveTopicTapped);
            CancelCommand = new DelegateCommand(OnCancelTapped);
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters.ContainsKey("FlashTopicKey"))
            {
                Title = "Details";
                ThisFlashTopic = parameters["FlashTopicKey"] as FlashTopic;
            }
            else
            {
                Title = "New Flash-Topic";
                ThisFlashTopic = new FlashTopic();
            }

            FlashTopicIdText = $"Flash Topic Id:  {ThisFlashTopic.Id}";
        }

        private void OnCancelTapped()
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(OnCancelTapped)}");

            NavigationService.GoBackAsync();
        }

        private async void OnSaveTopicTapped()
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(OnSaveTopicTapped)}:  {ThisFlashTopic}");

            ThisFlashTopic.Id = await _flashyRepository.SaveFlashTopic(ThisFlashTopic);
            await NavigationService.GoBackAsync();
        }
    }
}
