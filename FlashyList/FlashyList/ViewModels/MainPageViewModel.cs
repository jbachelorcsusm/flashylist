﻿using FlashyList.Models;
using FlashyList.Services;
using FlashyList.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FlashyList.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private IFlashyRepository _flashyRepository;

        public DelegateCommand AddTopicCommand { get; set; }
        public DelegateCommand<FlashTopic> TopicTappedCommand { get; set; }

        private ObservableCollection<FlashTopic> _topics;
        public ObservableCollection<FlashTopic> Topics
        {
            get { return _topics; }
            set { SetProperty(ref _topics, value); }
        }

        public MainPageViewModel(INavigationService navigationService,
                                IFlashyRepository flashyRepository)
            : base(navigationService)
        {
            _flashyRepository = flashyRepository;
            Title = "Flashy List!";
            AddTopicCommand = new DelegateCommand(OnAddTopicTapped);
            TopicTappedCommand = new DelegateCommand<FlashTopic>(OnTopicTapped);
        }

        private void OnTopicTapped(FlashTopic topicTapped)
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(OnTopicTapped)}:  {topicTapped}");

            NavigationParameters navParams = new NavigationParameters();
            navParams.Add("FlashTopicKey", topicTapped);
            NavigationService.NavigateAsync(nameof(NewTopicPage), navParams, false, true);
        }

        private async void RefreshFlashTopics()
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshFlashTopics)}");

            Topics = await _flashyRepository.GetAllFlashTopics();
        }

        private async void OnAddTopicTapped()
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(OnAddTopicTapped)}");

            await NavigationService.NavigateAsync(nameof(NewTopicPage), null, true, true);
        }

		public override void OnNavigatingTo(NavigationParameters parameters)
		{
            base.OnNavigatingTo(parameters);
            RefreshFlashTopics();
		}
	}
}
