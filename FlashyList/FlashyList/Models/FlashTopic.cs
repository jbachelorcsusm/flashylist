﻿using System;
using SQLite;

namespace FlashyList.Models
{
    [Table("flashtopic")]
    public class FlashTopic
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(250)]
        public string BriefDescription { get; set; }

        [MaxLength(1000)]
        public string Details { get; set; }

		public override string ToString()
		{
            return $"Id:{Id}, BriefDescription:{BriefDescription}, Details:{Details}";
		}
	}
}
